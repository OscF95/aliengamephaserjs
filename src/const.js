export const gameWidth = 960;
export const gameHeight = 480;
export const scale = 1;

export const normalTextStyle = {
  fontSize: 20,
  color: '#ffffff'
}

export const finalScoreStyle = {
  fontSize: 40,
  color: '#ffffff'
}