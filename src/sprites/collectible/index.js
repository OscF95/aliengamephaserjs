class Collectible extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, 'collectible');
    scene.add.existing(this);
    scene.physics.add.existing(this);
  }

  onCollected() {
    this.scene.score += 5;
    this.destroy();
  }
}

export default Collectible;