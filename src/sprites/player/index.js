import { gameHeight } from "../../const";

class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, 'player');

    this.initialPosition = {x, y};
    this.scene.add.existing(this);
    this.scene.physics.add.existing(this);
    this.setBounce(0.2);

    this.cursors = this.scene.input.keyboard.createCursorKeys();
  }

  update() {
    if (this.cursors.left.isDown) {
      this.setVelocityX(-200);
    } else if(this.cursors.right.isDown) {
      this.setVelocityX(200);
    }
    else {
      this.setVelocityX(0);
    }
    if (this.cursors.space.isDown && this.body.onFloor()) {
      this.setVelocityY(-300);
    }
    if (this.y > gameHeight)
    {
      this.resetPlayerPosition();
    }
  }

  resetPlayerPosition() {
    this.y = this.initialPosition.y;
    this.x = this.initialPosition.x;
  }
}

export default Player;