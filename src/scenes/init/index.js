import { scale, gameWidth, gameHeight, normalTextStyle, finalScoreStyle } from '../../const';
import map from '../../../assets/mapa.json';
import tile from '../../../assets/kenney_pixel-platformer/Tilemap/tiles_packed.png';
import player from '../../../assets/kenney_pixel-platformer/Characters/character_0001.png'
import collectible from '../../../assets/kenney_pixel-platformer/Tiles/tile_0067.png';
import Player from '../../sprites/player';
import Collectible from '../../sprites/collectible';


class Init extends Phaser.Scene {

  preload() {
    this.score = 0;
    this.load.tilemapTiledJSON('map', map);
    this.load.image('tileset', tile);
    this.load.image('player', player)
    this.load.image('collectible', collectible)
  }

  create() {
    this.collectiblesGroup = this.physics.add.group();
    this.map = this.make.tilemap({ key: 'map' });
    this.tileSet = this.map.addTilesetImage('tileset', 'tileset', 18, 18, 0, 0);
    this.platforms = this.map.createLayer('Plataforma', this.tileSet).setScale(scale);
    this.player = new Player(this, 100, 100).setScale(scale);
    this.platforms.setCollisionByExclusion([-1], true);
    this.physics.add.collider(this.player, this.platforms);
    this.scoreText = this.add.text(16, 16, 'Score: 0', normalTextStyle);

    for (let i = 0; i < 10; i++) {
      const x = Phaser.Math.Between(100, gameWidth - 50);
      const collectible = new Collectible(this, x, -100);
      this.collectiblesGroup.add(collectible);
    }

    this.physics.add.collider(this.collectiblesGroup, this.platforms);
    this.physics.add.collider(this.player, this.collectiblesGroup, this.collectibleCollisionHandler, null, this);
    this.input.keyboard.on('keydown-R', this.restartGame, this);
  }

  update() {
    this.player.update();

    this.collectiblesGroup.getChildren().forEach(collectible => {
      if (collectible.y > gameHeight + 100) {
        collectible.destroy();
      }
    })

    if (this.collectiblesGroup.getChildren().length === 0) {
      this.scene.pause();
      this.displayFinalScore();
      this.restartGame();
    }
  }

  collectibleCollisionHandler(player, collectible) {
    collectible.onCollected();
    this.scoreText.setText(`Score: ${this.score}`)
  }

  displayFinalScore() {
    const finalScoreText = this.add.text(gameWidth / 2, gameHeight / 2, 'Final Score: ' + this.score, finalScoreStyle);
    finalScoreText.setOrigin(0.5);
  }

  restartGame(input) {
    if (input === undefined) {
      setTimeout(() => {
        this.scene.restart();
      }, 5000)
    } else {
      this.scene.restart();
    }
  }
}

export default Init;
